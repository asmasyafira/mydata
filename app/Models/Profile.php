<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use SoftDeletes;
    protected $table = "profile";
    
    // public function get_profile(){
    //     return $this->belongsTo('App\\Models\\Profile','name','id');
    // }
}
